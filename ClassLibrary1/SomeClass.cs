﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary1
{
    public class AThirdClass
    {
        public int Num;
        public bool Boo;
    }
    public class AnotherClass
    {
        public List<AThirdClass> Item;
    }
    public class SomeClass
    {
        public void Method(IEnumerable<(int, bool)> e)
        {
            var z = new AnotherClass
            {
                Item = e.Select(x =>
                {
                    (var i, var b) = x;
                    return new AThirdClass
                    {
                        Num = i,
                        Boo = b
                    };
                }).ToList()
            };
        }
    }
}